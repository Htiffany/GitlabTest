package sample;
import static org.junit.Assert.*;
import org.junit.Test;
 
public class TaxTest {
	@Test
	public void test() {
		assertEquals(Tax.calcConsumptionTax(100), 8);
		assertEquals(Tax.calcConsumptionTax(500), 40);
		assertEquals(Tax.calcConsumptionTax(1000), 80);
	}
}

